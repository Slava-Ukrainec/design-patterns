const conductor = require ('./Conductor');
const {ExitCommand,CreateCommand} = require('./Command');

var { createInterface }  = require('readline');
var rl = createInterface({
    input: process.stdin,
    output: process.stdout
});

console.log('create <fileName> <text>| history | undo| redo | exit');
rl.prompt();

rl.on('line', input => {

    var [ commandText, fileName, ...fileText ] = input.split(' ')
    var text = fileText.join(' ');

    switch(commandText) {
        case "history":
            conductor.printHistory();
            break;

        case "redo":
            conductor.redo();
            break;
        case "undo":
            conductor.undo();
            break;

        case "exit":
            conductor.run (new ExitCommand());
            break;

        case "create" :
            conductor.run (new CreateCommand(fileName, text));
            console.log(`TODO: Create File ${fileName}`);
            console.log('file contents:', text);
            break;

        default :
            console.log(`${commandText} command not found!`);
    }

    rl.prompt();

});
