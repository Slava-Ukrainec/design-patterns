const LogStrategy = require('./LogStrategy');
const config = require('./config');
class Logger {
    constructor(strategy='toConsole') {
        this.logs = [];
        this.strategy = LogStrategy[strategy];
    }

    get count() {
        return this.logs.length
    }
    changeStrategy(newStrategyName) {
        this.strategy = LogStrategy[newStrategyName];
    }

    log(message) {
        const timestamp = new Date().toISOString();
        this.logs.push({ message, timestamp });
        this.strategy(timestamp,message)
    }

}

module.exports = new Logger(config.logs.strategy);
