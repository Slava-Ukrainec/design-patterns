class Mall {

    constructor() {
        this.sales = [];
    }

    notify(storName, discount){
        this.sales.push({storName,discount});
    }

}

module.exports = Mall;
