const Person = require('./Person');

class PersonBuilder {
    constructor(name) {
        this.name = name;
    }

    makeEmployee(hours = 40) {
        this.isEmployee = true;
        this.hours = hours;
        return this;
    }

    makeManager() {
        this.isManager = true;
        return this;
    }
    makePartTime(hours = 20){
        this.hours = hours;
        return this;
    }
    withMoney(money){
        this.money = money;
        return this;
    }
    withList(list=[]){
        this.shoppingList = list;
        return this;
    }
    build(){
        return new Person(this)
    }
}
module.exports = PersonBuilder;