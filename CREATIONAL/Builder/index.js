var PersonBuilder = require('./PersonBuilder');

// Employees
var sue = new PersonBuilder('Sue').makeEmployee(60).makeManager().build();
var bill = new PersonBuilder('Bill').makeEmployee().makePartTime().build();

// Shoppers
var charles = new PersonBuilder('Charles')
    .withMoney(500)
    .withList( ['jeans', 'sunglasses'])
    .build();
var tabbitha = new PersonBuilder('Tabbitha').withMoney(1000).build();

console.log( sue.toString() );
console.log( bill.toString() );
console.log( charles.toString() );